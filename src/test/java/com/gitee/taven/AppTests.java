package com.gitee.taven;

import com.gitee.taven.utils.RedisLock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTests {

	@Autowired
	private RedisLock redisLock;
	@Test
	public void contextLoads() {
		boolean lock = redisLock.tryLock("/submit", "ab", 30);
		boolean lock1 = redisLock.tryLock("/submit", "abc", 30);
		boolean lock2 = redisLock.tryLock("/submit1", "ab", 30);
		boolean lock3 = redisLock.tryLock("/submit1", "abc", 30);
		boolean lock4 = redisLock.tryLock("/submit", "ab", 30);
		System.out.println(lock + "-" + lock1 + "-" + lock2 + "-" + lock3 + "-" + lock4);
	}

}
